from rest_framework import serializers
from .models import Fillup


class FillupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fillup
        fields = (
            "car",
            "odometer",
            "volume",
            "price_per_unit",
            "full_tank",
            "date",
            "updated",
            "timestamp",
        )
