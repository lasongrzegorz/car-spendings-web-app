from django.db import models
from cars.models import Car
from .managers import FillupManager


class Fillup(models.Model):
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    odometer = models.PositiveSmallIntegerField()
    volume = models.FloatField()
    price_per_unit = models.FloatField()
    full_tank = models.BooleanField(default=False)
    date = models.DateTimeField(
        auto_now=False, auto_now_add=False, null=True, blank=True
    )
    updated = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    objects = FillupManager()

    def __str__(self):
        return f"{self.date} - volume: {self.volume}"

    @property
    def total_price(self):
        return self.volume * self.price_per_unit
