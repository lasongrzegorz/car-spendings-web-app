from django import forms
from cars.models import Car

from .models import Fillup
from crispy_forms.helper import FormHelper
from crispy_forms.layout import (
    Layout,
    Fieldset,
    ButtonHolder,
    Submit,
    Field,
    Div,
)
from crispy_forms.bootstrap import AppendedText


class FillupModelForm(forms.ModelForm):
    # hide 'car' field for Fillup add/update
    # add car_id to each fillup form
    def __init__(self, car_id, previous_fillup=None, update=False, *args, **kwargs):
        super(FillupModelForm, self).__init__(*args, **kwargs)
        self.car_id = car_id
        self.update = update
        self.previous_fillup = previous_fillup
        self.fields["car"].initial = Car.objects.get(id=car_id)
        self.fields["price_per_unit"].label = "Price"
        self.fields["full_tank"].label = "Full"
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Fieldset("Add new fillup"),
            Field("car", type="hidden"),
            Div(
                Div("date", css_class="form-group col-md-2 mb-0"),
                Div("volume", css_class="form-group col-md-2 mb-0"),
                Div("price_per_unit", css_class="form-group col-md-2 mb-0"),
                Div("odometer", css_class="form-group col-md-2 mb-0"),
                Div("full_tank", css_class="form-check col-auto mb-0"),
                ButtonHolder(
                    Submit("submit", "Add fillup", css_class="btn btn-primary")
                ),
                css_class="form-row align-items-center",
            ),
        )

    class Meta:
        model = Fillup
        fields = [
            "car",
            "odometer",
            "volume",
            "price_per_unit",
            "date",
            "full_tank",
        ]

    def clean_odometer(self):
        user_odometer = self.cleaned_data.get("odometer")

        if not self.update:
            qs = Fillup.objects.get_car_last_fillup(self.car_id)
            if qs:
                last_odometer = qs.odometer
                if user_odometer < last_odometer:
                    raise forms.ValidationError(
                        f"Odometer value cannot be lower than last input of {last_odometer}"
                    )
            else:
                if user_odometer < 0:
                    raise forms.ValidationError(
                        f"Odometer value cannot be lower than 0"
                    )
        else:
            previous_fillup = self.previous_fillup.odometer
            if previous_fillup:
                prev_odometer = previous_fillup
                if user_odometer < prev_odometer:
                    raise forms.ValidationError(
                        f"Odometer value cannot be lower than previous input of {prev_odometer}"
                    )
            else:
                if user_odometer < 0:
                    raise forms.ValidationError(
                        f"Odometer value cannot be lower than 0"
                    )

        return user_odometer
