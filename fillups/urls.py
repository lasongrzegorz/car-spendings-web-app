from django.urls import path
from .views import (
    FillupListView,
    FillupUpdateView,
    FillupDeleteView,
)

app_name = "fillups"

urlpatterns = [
    path("", FillupListView.as_view(), name="list"),
    path("add_fillup/", FillupListView.as_view(), name="add_fillup"),
    path(
        "<int:fillup_id>/update_fillup/",
        FillupUpdateView.as_view(),
        name="update_fillup",
    ),
    path(
        "<int:fillup_id>/delete_fillup/",
        FillupDeleteView.as_view(),
        name="delete_fillup",
    ),
]
