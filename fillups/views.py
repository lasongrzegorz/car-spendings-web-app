from django.contrib import messages
from django.shortcuts import redirect, reverse
from django.urls import reverse_lazy
from django.views.generic import ListView, DeleteView, UpdateView

from cars.models import Car
from fillups.models import Fillup
from fillups.forms import FillupModelForm


class FillupListView(ListView):
    model = Fillup
    template_name = "fillups/fillups_list.html"
    context_object_name = "fillups"

    def get_queryset(self):
        return Fillup.objects.filter(car_id=self.kwargs["car_id"])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        car = Car.objects.get(id=self.kwargs["car_id"])
        form = FillupModelForm(car_id=self.kwargs["car_id"])
        context["car"] = car
        context["form"] = form
        return context

    def post(self, request, *args, **kwargs):
        car_id = self.kwargs["car_id"]
        form = FillupModelForm(car_id=car_id, data=request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse("cars:fillups:list", kwargs={"car_id": car_id}))


class FillupUpdateView(UpdateView):
    template_name = "fillups/fillup_update.html"
    fields = ["car", "odometer", "volume", "price_per_unit", "date", "full_tank"]
    success_url = reverse_lazy("cars:fillups:list")

    def get_queryset(self):
        fillup_id = self.kwargs.get("fillup_id")
        return Fillup.objects.filter(id=fillup_id)

    def get_object(self, queryset=None):
        return Fillup.objects.get(id=self.kwargs["fillup_id"])

    def get_context_data(self, **kwargs):
        fillup = self.get_object()
        previous_fillup = Fillup.objects.get_car_previous_fillup(
            fillup.car.id, fillup.id
        )
        form = FillupModelForm(
            instance=fillup,
            car_id=fillup.car.id,
            update=True,
            previous_fillup=previous_fillup,
        )
        context = {
            "form": form,
            "fillup": fillup,
            "car": fillup.car,
        }
        return super().get_context_data(**context)

    def post(self, request, *args, **kwargs):
        fillup = self.get_object()
        previous_fillup = Fillup.objects.get_car_previous_fillup(
            fillup.car.id, fillup.id
        )
        form = FillupModelForm(
            data=request.POST,
            instance=fillup,
            car_id=fillup.car.id,
            update=True,
            previous_fillup=previous_fillup,
        )
        if form.is_valid():
            form.save()
            return redirect(
                reverse_lazy("cars:fillups:list", kwargs={"car_id": fillup.car.id})
            )
        else:
            messages.error(
                self.request,
                "Fill up has not been updated. "
                "Odometer value cannot be lower than previous input",
            )
            return redirect(
                reverse_lazy("cars:fillups:list", kwargs={"car_id": fillup.car.id})
            )


class FillupDeleteView(DeleteView):
    model = Fillup
    pk_url_kwarg = "fillup_id"

    def get_success_url(self):
        car_id = self.kwargs["car_id"]
        return reverse_lazy("cars:fillups:list", kwargs={"car_id": car_id})
