from django.test import SimpleTestCase
from django.urls import reverse, resolve
from fillups.views import (
	fillup_view,
	fillup_add_view,
	fillup_delete_view,
	fillup_update_view,
)


class TestFillupsUrls(SimpleTestCase):

	def test_list_url_resolves(self):
		url = reverse('fillups:list', args=[1])
		self.assertEquals(resolve(url).func, fillup_view)

	def test_add_url_resolves(self):
		url = reverse('fillups:add_fillup', args=[1])
		self.assertEquals(resolve(url).func, fillup_add_view)

	def test_delete_url_resolves(self):
		url = reverse('fillups:delete_fillup', args=[1, 1])
		self.assertEquals(resolve(url).func, fillup_delete_view)

	def test_update_url_resolves(self):
		url = reverse('fillups:update_fillup', args=[1, 1])
		self.assertEquals(resolve(url).func, fillup_update_view)