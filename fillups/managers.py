from django.db import models


class FillupQuerySet(models.QuerySet):
    def car_fillups(self, car_id):
        return self.filter(car_id=car_id)

    def car_fillups_lt(self, car_id, edited_fillup_id):
        return self.filter(car_id=car_id, id__lt=edited_fillup_id)

    def by_id(self):
        return self.order_by("id")


class FillupManager(models.Manager):
    def get_queryset(self):
        return FillupQuerySet(self.model, using=self._db)

    def get_car_last_fillup(self, car_id):
        return self.get_queryset().car_fillups(car_id).by_id().last()

    def get_car_previous_fillup(self, car_id, edited_fillup_id):
        return (
            self.get_queryset().car_fillups_lt(car_id, edited_fillup_id).by_id().last()
        )
