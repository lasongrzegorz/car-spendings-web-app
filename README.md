# Car Costs App
This is an app that allows controlling costs like fuel, car repair, cleaning
and other expenditures for a given car. 


## Table of contents
* [General info](#general-info)
* [Setup](#setup)
* [To-Do](#to-do-list)


## General info
The idea is to have entire history of car costs in one place together with
 statistics and reports of car expenditures.
Currently a logged in user is able to add a car and respective fuel costs
. Also only logged in user is able to view/add/modify cars, which one added.

Going forward a user will be able to add different costs categories and
 check car reports/history
 

## Setup
App is being built using Django. Apart from regular dependencies, one should
 install django-crispy-forms (check requirements.txt for details).
 
 
## To-do list:
* Other than fuel expenditures
* Car statistics and graphs
* Car reports generation
* Transfer to REST Api
* Add tests
  