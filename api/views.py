from django.contrib.auth import get_user_model
from rest_framework import viewsets
from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response

from cars.models import Car
from fillups.models import Fillup
from .serializers import (
    CarSerializer,
    AdminCarSerializer,
    FillupSerializer,
    AdminFillupSerializer,
    AccountSerializer,
)


class CarViewSet(viewsets.ModelViewSet):
    lookup_field = "id"

    def get_serializer_class(self):
        if self.request.user.is_superuser:
            return AdminCarSerializer
        return CarSerializer

    def get_queryset(self):
        if self.request.user.is_superuser:
            return Car.objects.all()
        return Car.objects.filter(user=self.request.user)


class FillupViewSet(viewsets.ModelViewSet):
    def get_serializer_class(self):
        if self.request.user.is_superuser:
            return AdminFillupSerializer
        return FillupSerializer

    def get_queryset(self):
        if self.request.user.is_superuser:
            return Fillup.objects.all()
        return Fillup.objects.filter(user=self.request.user)


class AccountRetrieveView(RetrieveAPIView):
    def retrieve(self, request, pk=None, *args, **kwargs):
        user = get_user_model()
        serializer = AccountSerializer(user.objects.get(pk=request.user.pk))
        return Response(serializer.data)
