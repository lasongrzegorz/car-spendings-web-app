from django.urls import path, include
from rest_framework import routers

from . import views


router = routers.DefaultRouter()
router.register("cars", views.CarViewSet, basename="cars")
router.register("fillups", views.FillupViewSet, basename="fillups")

urlpatterns = [
    path("", include(router.urls)),
    path("profile/", views.AccountRetrieveView.as_view(), name="profile"),
]
