from rest_framework import serializers
from django.contrib.auth import get_user_model

from cars.models import Car
from fillups.models import Fillup


class AdminCarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = [
            "id",
            "brand",
            "model",
            "year",
            "mileage",
            "user",
        ]


class CarSerializer(AdminCarSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault(),)


class AdminFillupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fillup
        fields = [
            "id",
            "car",
            "odometer",
            "volume",
            "price_per_unit",
            "full_tank",
            "date",
            "updated",
            "timestamp",
            "total_price",
        ]


class FillupSerializer(AdminFillupSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault(),)


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = [
            "id",
            "email",
            "date_joined",
            "last_login",
        ]
