from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group
from accounts.models import Account


class AccountAdmin(UserAdmin):

    class Meta:
        model = Account

    search_fields = ["email"]
    date_hierarchy = "date_joined"
    list_display = [
        "email",
        "admin",
        "staff",
        "active",
        "last_login",
        "date_joined",
    ]
    list_editable = [
        "active",
    ]
    list_filter = ["active", "staff", "admin"]
    readonly_fields = [
        "date_joined",
        "last_login",
    ]
    filter_horizontal = []
    ordering = ["email"]

    fieldsets = (
        (None, {"fields": ("email", "password")}),
        ("Permissions", {"fields": ("admin", "staff", "active")}),
        ("Profile info", {"fields": ()}),
    )
    add_fieldsets = (
        (None, {"fields": ("email", "password1", "password2")}),
    )


admin.site.register(Account, AccountAdmin)
admin.site.unregister(Group)
