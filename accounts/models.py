from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


class AccountManager(BaseUserManager):
    def create_user(
        self, email, password=None, is_active=True, is_staff=False, is_admin=False
    ):
        if not email:
            raise ValueError("User must have an email address")
        if not password:
            raise ValueError("User must have a password")

        account_obj = self.model(email=self.normalize_email(email),)
        account_obj.set_password(password)
        account_obj.active = is_active
        account_obj.admin = is_admin
        account_obj.staff = is_staff
        account_obj.save(using=self._db)
        return account_obj

    def create_staffuser(self, email, password=None):
        account_obj = self.create_user(email, password=password, is_staff=True)
        account_obj.save(using=self._db)
        return account_obj

    def create_superuser(self, email, password=None):
        account_obj = self.create_user(
            email, password=password, is_staff=True, is_admin=True
        )
        account_obj.save(using=self._db)
        return account_obj


class Account(AbstractBaseUser):
    email = models.EmailField(max_length=255, unique=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    last_login = models.DateTimeField(auto_now=True)
    admin = models.BooleanField(default=False)  # superuser
    active = models.BooleanField(default=True)
    staff = models.BooleanField(default=False)

    # define the login field. Usually username or email
    USERNAME_FIELD = "email"
    # field required on login, that user cannot register without it
    # USERNAME_FIELD and password are required by default
    REQUIRED_FIELDS = []

    objects = AccountManager()

    def __str__(self):
        return self.email

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_admin(self):
        return self.admin

    @property
    def is_staff(self):
        return self.staff

    @property
    def is_active(self):
        return self.active

    @property
    def is_superuser(self):
        return self.admin


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

# # for future work to get user details
# class Profile(models.Model):
# 	account = models.OneToOneField(Account)
# 	full_name = models.CharField(max_length=255, blank=True, null=True)
# 	# extend user details in the future
