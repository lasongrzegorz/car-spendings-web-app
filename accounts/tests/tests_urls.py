from django.test import SimpleTestCase
from django.urls import reverse, resolve
from accounts.views import (
	registration_view,
	login_view,
	logout_view,
)


class TestFillupsUrls(SimpleTestCase):

	def test_register_url_resolves(self):
		url = reverse('register')
		self.assertEquals(resolve(url).func, registration_view)

	def test_login_url_resolves(self):
		url = reverse('login')
		print(resolve(url))
		self.assertEquals(resolve(url).func, login_view)

	def test_logout_url_resolves(self):
		url = reverse('logout')
		self.assertEquals(resolve(url).func, logout_view)
