from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth.hashers import check_password

from .models import Account


class LoginForm(forms.Form):
    email = forms.EmailField(
        label="Email", widget=forms.EmailInput(attrs={"placeholder": "Email"})
    )

    password = forms.CharField(
        label="Password", widget=forms.PasswordInput(attrs={"placeholder": "Password"})
    )

    def clean_email(self):
        # Check that email exists
        email = self.cleaned_data.get("email")
        qs = Account.objects.filter(email=email)
        if len(qs) != 1:
            raise forms.ValidationError("Email is invalid")
        return email

    def clean_password(self):
        password = self.cleaned_data.get("password")
        email = self.cleaned_data.get("email")
        qs = Account.objects.filter(email=email).first()
        if qs is not None:
            if not check_password(password, qs.password):
                raise forms.ValidationError("Password does not match")
        else:
            raise forms.ValidationError("Password is invalid")
        return password

    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)


class RegisterForm(forms.ModelForm):
    password = forms.CharField(
        label="Password", widget=forms.PasswordInput(attrs={"placeholder": "Password"})
    )
    password2 = forms.CharField(
        label="Confirm password",
        widget=forms.PasswordInput(attrs={"placeholder": "Confirm password"}),
    )

    class Meta:
        model = Account
        fields = ("email",)
        widgets = {"email": forms.TextInput(attrs={"placeholder": "Email"})}

    def clean_email(self):
        email = self.cleaned_data.get("email")
        qs = Account.objects.filter(email=email)
        if qs.exists():
            raise forms.ValidationError("Email " + email + " is taken")
        return email

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(RegisterForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


class AccountAdminCreationForm(forms.ModelForm):
    """
    A form for creating new users. Includes all the required
    fields, plus a repeated password.
    """

    password1 = forms.CharField(label="Password", widget=forms.PasswordInput)
    password2 = forms.CharField(label="Confirm password", widget=forms.PasswordInput)

    class Meta:
        model = Account
        fields = ("email",)

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(AccountAdminCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserAdminChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """

    password = ReadOnlyPasswordHashField()

    class Meta:
        model = Account
        fields = ("email", "password", "active", "admin")

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        return self.initial["password"]
