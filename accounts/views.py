from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout, views

from .forms import RegisterForm, LoginForm


def registration_view(request):

    form = RegisterForm()
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            email = form.cleaned_data.get("email")
            messages.success(request, "Account created for " + email + ". Please login")
            return redirect("login")
        else:
            email = form["email"].value()
            messages.info(request, "Email " + str(email) + " is taken")
            return redirect("register")

    template = "accounts/registration.html"
    context = {"register_form": form}
    return render(request, template, context)


class AccountLoginView(views.LoginView):
    template_name = "accounts/login.html"
    form_class = LoginForm

    def form_valid(self, form):
        email = form.cleaned_data.get("email")
        password = form.cleaned_data.get("password")

        account = authenticate(email=email, password=password)
        if account:
            if account.active:
                login(self.request, account)
                return redirect(self.get_success_url())
        else:
            return self.form_invalid()


def login_view(request):

    form = LoginForm()
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data.get("email")
            password = form.cleaned_data.get("password")

            account = authenticate(request, email=email, password=password)
            if account:
                login(request, account)
                return redirect("index")
        else:
            messages.info(request, "Email or password is incorrect")
            return redirect("login")

    context = {"login_form": form}
    template = "accounts/login.html"
    return render(request, template, context)


def logout_view(request):
    logout(request)
    return redirect("login")
