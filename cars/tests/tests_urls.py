from django.test import SimpleTestCase
from django.urls import reverse, resolve
from cars.views import (
	cars_view,
	car_add_view,
	car_delete_view,
	car_update_view,
)


# SimpleTestCase if you do not need to interact with db
class TestCarsUrls(SimpleTestCase):

	def test_list_url_resolves(self):
		url = reverse('cars:list')
		self.assertEquals(resolve(url).func, cars_view)

	def test_add_url_resolves(self):
		url = reverse('cars:add_car')
		self.assertEquals(resolve(url).func, car_add_view)

	def test_delete_url_resolves(self):
		url = reverse('cars:delete_car', args=[1])
		self.assertEquals(resolve(url).func, car_delete_view)

	def test_update_url_resolves(self):
		url = reverse('cars:update_car', args=[1])
		self.assertEquals(resolve(url).func, car_update_view)