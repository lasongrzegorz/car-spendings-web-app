from django.urls import path, include
from .views import (
    CarsListView,
    CarDetailView,
    CarDeleteView,
    CarUpdateView,
)

app_name = "cars"

urlpatterns = [
    path("", CarsListView.as_view(), name="list"),
    path("<int:car_id>/", CarDetailView.as_view(), name="car_details"),
    path("add_car/", CarsListView.as_view(), name="add_car"),
    path("update_car/<int:car_id>/", CarUpdateView.as_view(), name="update_car"),
    path("delete_car/<int:car_id>/", CarDeleteView.as_view(), name="delete_car"),
    # Fillups
    path("<int:car_id>/fillups/", include("fillups.urls")),
]
