from django.conf import settings
from django.db import models
from accounts.models import Account


class Car(models.Model):
    brand = models.CharField(max_length=200)
    model = models.CharField(max_length=200)
    year = models.PositiveSmallIntegerField(blank=True, null=True)
    mileage = models.PositiveSmallIntegerField(blank=True, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.brand} {self.model}"


class CarUnits(models.Model):
    DISTANCE_UNITS_CHOICE = {
        ("kilometers", "kilometers"),
        ("miles", "miles"),
    }
    FUEL_UNITS_CHOICE = {
        ("liters", "liters"),
        ("gallons", "gallons"),
    }

    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    distance_unit = models.CharField(max_length=64, choices=DISTANCE_UNITS_CHOICE)
    fuel_unit = models.CharField(max_length=64, choices=FUEL_UNITS_CHOICE)

    def __str__(self):
        return f"{self.fuel_economy_unit}"

    @property
    def fuel_economy_unit(self):
        if (
            self.DISTANCE_UNITS_CHOICE == "kilometers"
            and self.FUEL_UNITS_CHOICE == "liters"
        ):
            return f"l/100km"
        elif (
            self.DISTANCE_UNITS_CHOICE == "kilometers"
            and self.FUEL_UNITS_CHOICE == "gallons"
        ):
            return f"km/gal"
        elif (
            self.DISTANCE_UNITS_CHOICE == "miles" and self.FUEL_UNITS_CHOICE == "liters"
        ):
            return f"l/100mi"
        elif (
            self.DISTANCE_UNITS_CHOICE == "miles"
            and self.FUEL_UNITS_CHOICE == "gallons"
        ):
            return f"MPG"
        else:
            return None
