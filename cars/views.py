from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseForbidden
from django.shortcuts import render, reverse, redirect
from django.urls import reverse_lazy
from django.views.generic import (
    View,
    ListView,
    DetailView,
    DeleteView,
    UpdateView,
)
from django.views.generic.edit import FormMixin

from accounts.models import Account
from cars.models import Car
from cars.forms import CarModelForm
from fillups.models import Fillup


class IndexView(View):
    def get(self, request, *args, **kwargs):
        template = "cars/pages/index.html"
        return render(request, template)


class CarsListView(LoginRequiredMixin, FormMixin, ListView):
    template_name = "cars/pages/car_list.html"
    form_class = CarModelForm
    success_url = reverse_lazy("cars:list")
    context_object_name = "cars"

    def get_queryset(self):
        return Car.objects.filter(user=self.request.user)

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseForbidden()
        form = self.get_form()
        if form.is_valid():
            form.save(commit=False)
            user = self.request.user
            form.instance.user = Account.objects.get(id=user.id)
            form.save()
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class CarDetailView(LoginRequiredMixin, DetailView):
    model = Car
    template_name = "cars/pages/car_details.html"
    context_object_name = "car"
    pk_url_kwarg = "car_id"

    def get_object(self, queryset=None):
        return Car.objects.get(id=self.kwargs["car_id"])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        car = Car.objects.get(id=self.kwargs["car_id"])
        try:
            fillups = Fillup.objects.filter(car=self.get_object()).latest("id")
        except Fillup.DoesNotExist:
            fillups = None
        context["car"] = car
        context["fillups"] = fillups
        return context


class CarUpdateView(UpdateView):
    template_name = "cars/pages/car_update.html"
    fields = ["brand", "model", "year", "mileage"]
    success_url = reverse_lazy("cars:list")

    def get_queryset(self):
        car_id = self.kwargs.get("car_id")
        return Car.objects.filter(id=car_id)

    def get_object(self, queryset=None):
        return Car.objects.get(id=self.kwargs["car_id"])

    def get_context_data(self, **kwargs):
        form = CarModelForm(instance=self.get_object())
        context = {
            "form": form,
        }
        return super().get_context_data(**context)


class CarDeleteView(DeleteView):
    model = Car
    pk_url_kwarg = "car_id"
    success_url = reverse_lazy("cars:list")
