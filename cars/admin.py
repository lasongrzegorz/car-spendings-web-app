from django.contrib import admin
from cars.models import Car


class CarAdmin(admin.ModelAdmin):

    class Meta:
        model = Car


admin.site.register(Car)
